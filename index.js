console.log("Hello World");

// [SECTION] Conditional Statement
// If, else if, else

let numG = -1;

// If statement
// Executes a statement if a specified condition is true

if(numG < 0) {
    console.log("Hello");
}

if(numG < 0) {
    console.log("Hello");
}

let numH = 1;

// else if clause
// Executes a statement if previous conditions are false and if the specified condition is true.

if(numG > 0) {
    console.log("Hello");
} 
else if(numH > 0) {
    console.log("World");
}

// else statement
// Executes a statement if all other conditions are false
// The "else" statement is optional and can be added to capture any other result to change the result of the flow of a program

if(numG > 0) {
    console.log("Hello");
}
else if(numH = 0) {
    console.log("World");
}
else {
    console.log("Again");
}

// If, else if, else statement with a function

let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windSpeed) {
    if (windSpeed < 30){
        return "Not a typhoon yet.";
    } else if(windSpeed <= 61) {
        return "Tropical Depression detected.";
    } else if(windSpeed >= 62 && windSpeed <= 88) {
        return "Tropical Storm detected!";
    } else if(windSpeed >= 89 && windSpeed <= 177) {
        return "Severe tropical storm detected!!";
    } else {
        return "Typhoon detected!!!";
    }
}

// Returns the string to the variable "message" that invoked it

message = determineTyphoonIntensity(69);
console.log(message);

message = determineTyphoonIntensity(27);
console.log(message);

message = determineTyphoonIntensity(200);
console.log(message);

message = determineTyphoonIntensity(100);
console.log(message);

message = determineTyphoonIntensity(178);
console.log(message);

if(message == "Tropical Storm detected!") {
    console.log(message);
}

message = determineTyphoonIntensity(75);
console.warn(message); 

// [SECTION] Truthy and Falsy

//truthy examples 
if(true) {
    console.log("Truthy");
}

if(1) {  // 1 = true, hence it shows the message invkoed below
    console.log("Truthy");
}

if([]) {
    console.log("Truthy");
}

// falsy examples
if(false) {
    console.log("Falsy");
}

if(0) { // 0 == false
    console.log("Falsy");
}

if(undefined) {
    console.log("Falsy");
}

// [SECTION] Conditional (Ternary) Operator
// Conditional (Ternary) Operator takes in three operands: condition; expression to execute if the condition is truthy; expression to execute if the condition is falsy.

// Single staement execution
let ternaryResult = (1 < -1) ? "Hello" : "World"; // ? --> if true; : --> if false
console.log("Result of ternary operator: " + ternaryResult);

// Multiple statement execution
let name;

function isOfLegalAge() {
    name = "John";
    return "You are of legal age limit";
}

function isUnderAge() {
    name = "Jane";
    return "You are under the age limit";
}

let age = parseInt(prompt("What is your age?")); // parseInt converts a string number into an integer.
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge(); 
console.log("Result of Ternary Operator in function: " + legalAge + ", " + name);

// [SECTION] Switch Statement

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
    case "monday":
        console.log("The color of the day is red");
        break;
    
    case "tuesday":
        console.log("The color of the day is orange");
        break;

    case "wednesday":
        console.log("The color of the day is yellow");
        break;

    case "thursday":
        console.log("The color of the day is green");
        break;
    
    case "friday":
        console.log("The color of the day is blue");
        break;

    case "thursday":
        console.log("The color of the day is indigo");
        break;
    
    case "sunday":
        console.log("The color of the day is violet");
        break;

    default:
        console.log("Please input a valid day");
        break;
}

// [SECTION] Try-Catch-Finally Statement

function showIntensityAlert(windSpeed) {
    try {
        // attempt to execute a code
        alert(determineTyphoonIntensity(windSpeed));
    }
    catch (error) {
        console.log(typeof error);
        console.warn(error.message);
    }
    finally {
        alert("Intensity updates will show new alert.");
    }
}
showIntensityAlert(56);